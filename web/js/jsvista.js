
function definirTamanos(){
    
  var numero=document.getElementById("entrada").value;
  var label="";
  var select="";
  var div="";
  var rtas="";
  
  var espacioNumero= document.getElementById("entrada");
  var btnNumero=document.getElementById("btnentrada");


if(numero>0 ){
  for(var i=1;i<=numero;i++){
    rtas+="<div id='rta"+i+"'> </div>";
  }
  document.getElementById("rtas").innerHTML=rtas;
  
   for(var i=1;i<=numero;i++){
       
        div="<div class='container mt-3 bg-light rounded border border-danger'>";
        label="<label class='pl=5 mt-5'> Tamaño de pizza "+i+"</label>";
    select = "<select name='pizzasTam"+i+"'>";
    select+="<option value='pequena'> pequena </option>";
    select+="<option value='mediana'> mediana </option>";
    select+="<option value='grande'> grande </option>";
    select+="</select>";
    div+=label+select+"</div>";
    
    
    document.getElementById("rta"+i).innerHTML=div;

      }
  }
    else
    {
      div="<div class='container mt-3 bg-warning rounded border border-danger'>";
      div+="<h3>NO se aceptan pedidos negativos</h3></div>";
    document.getElementById("rtas").innerHTML=div;

    }
    //espacioNumero.disabled  = true;
    btnNumero.disabled  = true;

}

function definirAdicionales()
{
    
    var numPizzas = document.getElementById("cantPizzas").value;
    var adi1="";
    var adi2="";
    var ninguno="Ninguno";
    for(var i=1;i<=numPizzas;i++)
    {
            var saborPizza1 = document.getElementById("sabores1_"+i).value;
            var saborPizza2 = document.getElementById("sabores2_"+i).value;
            adi1="<label class='mt-3'>Ingredientes adicionales( pizza "+saborPizza1+")";
            adi1+="<br><input type='checkbox' name='tocineta1' value='tocineta'>Tocineta  <input type='checkbox' name='salami1' value='salami'>Salami  <input type='checkbox' name='oregano1' value='oregano'>Oregano  <input type='checkbox' name='salchicha1' value='salchicha'>Salchicha  ";
            if(saborPizza2!==ninguno)
            {
            adi2="<label class='mt-3'>Ingredientes adicionales( pizza "+saborPizza2+")";
            adi2+="<br><input type='checkbox' name='tocineta2' value='tocineta'>Tocineta<input type='checkbox' name='salami2' value='salami'>Salami<input type='checkbox' name='oregano2' value='oregano'>Oregano<input type='checkbox' name='salchicha2' value='salchicha'>Salchicha";
            }
            
            document.getElementById("adicionales1_"+i).innerHTML=adi1;
            document.getElementById("adicionales2_"+i).innerHTML=adi2;
    }
    
}

function cambiarAdicionales(num)
{
    var adi1="";
    var adi2="";
    var ninguno="Ninguno";
     var saborPizza1 = document.getElementById("sabores1_"+num).value;
     var saborPizza2 = document.getElementById("sabores2_"+num).value;
            adi1="<label class='mt-3'>Ingredientes adicionales( pizza "+saborPizza1+")";
            adi1+="<br><input type='checkbox' name='tocineta1' value='tocineta'>Tocineta  <input type='checkbox' name='salami1' value='salami'>Salami  <input type='checkbox' name='oregano1' value='oregano'>Oregano  <input type='checkbox' name='salchicha1' value='salchicha'>Salchicha  ";
            if(saborPizza2!==ninguno)
            {
            adi2="<label class='mt-3'>Ingredientes adicionales( pizza "+saborPizza2+")";
            adi2+="<br><input type='checkbox' name='tocineta2' value='tocineta'>Tocineta<input type='checkbox' name='salami2' value='salami'>Salami<input type='checkbox' name='oregano2' value='oregano'>Oregano<input type='checkbox' name='salchicha2' value='salchicha'>Salchicha";
            }
            document.getElementById("adicionales1_"+num).innerHTML=adi1;
            document.getElementById("adicionales2_"+num).innerHTML=adi2;
}

function definirImagenes()
{
    var numPizzas = document.getElementById("cantPizzas").value;
    var imag1="";
    var imag2="";
    var sabor1 = "Hawayana";
    var sabor2 = "Mexicana";
    var sabor3 = "Napolitana";
    var sabor4 = "Vegetariana";
    
    for(var i=1;i<=numPizzas;i++)
    {
        var saborPizza1 = document.getElementById("sabores1_"+i).value;
        var saborPizza2 = document.getElementById("sabores2_"+i).value;
        if(saborPizza1===sabor1){
            imag1="<img src='./imagenes/imghawaiana.png' width='180' height='200' >";
        }else{
            if(saborPizza1===sabor2){
                imag1="<img src='./imagenes/imgmexicana.png' width='180' height='200'>"; 
            }else{
                if(saborPizza1===sabor3){
                    imag1="<img src='./imagenes/imgnapolitana.png' width='180' height='200'>";
                }else{
                    if(saborPizza1===sabor4){
                        imag1="<img src='./imagenes/imgvegetariana.png' width='180' height='200'>";
                    }
                }
            }
        }
        document.getElementById("imagen1_"+i).innerHTML=imag1;
        document.getElementById("imagen2_"+i).innerHTML=imag2; 
    }
 }   
    
function cambiarimagenes(num)
{
    var imag1="";
    var imag2="";
    var ninguno="Ninguno";
    var sabor1 = "Hawayana";
    var sabor2 = "Mexicana";
    var sabor3 = "Napolitana";
    var sabor4 = "Vegetariana";
    var saborPizza1 = document.getElementById("sabores1_"+num).value;
    var saborPizza2 = document.getElementById("sabores2_"+num).value;
    if(saborPizza1===sabor1){
            imag1="<img src='./imagenes/imghawaiana.png' width='180' height='200' >";
        }else{
            if(saborPizza1===sabor2){
                imag1="<img src='./imagenes/imgmexicana.png' width='180' height='200' >";
            }else{
                if(saborPizza1===sabor3){
                    imag1="<img src='./imagenes/imgnapolitana.png' width='180' height='200' >";
                }else{
                    if(saborPizza1===sabor4){
                        imag1="<img src='./imagenes/imgvegetariana.png' width='180' height='200' >";
                    }
                }
            }
        }
    if(saborPizza2!==ninguno){
        if(saborPizza2===sabor1){
            imag2="<img src='./imagenes/imghawaiana.png' width='180' height='200' >";
        }else{
            if(saborPizza2===sabor2){
                imag2="<img src='./imagenes/imgmexicana.png' width='180' height='200' >";
            }else{
                if(saborPizza2===sabor3){
                    imag2="<img src='./imagenes/imgnapolitana.png' width='180' height='200'>";
                }else{
                    if(saborPizza2===sabor4){
                        imag2="<img src='./imagenes/imgvegetariana.png' width='180' height='200'>";
                    }
                }
            }
        }
    }
    document.getElementById("imagen1_"+num).innerHTML=imag1;
    document.getElementById("imagen2_"+num).innerHTML=imag2; 
}



