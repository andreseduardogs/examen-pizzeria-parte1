<%-- 
    Document   : procesarFactura
    Created on : 21/12/2020, 02:03:28 PM
    Author     : Maggy
--%>

<%@page import="java.util.List"%>
<%@page import="DTO.Pizza"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/bootstrap.min.css" >
        <script src="./js/jsvista.js"></script>
        <title>Pizzeria el Tio Steve</title>
    </head>
    <body style="background-color:#F9CD8E;">
        
        <div class="row">
    <div class="col-sm mt-3 pl-5">
    <img src="./imagenes/img1logo.png" width="250" height="204">
    </div>
    <div class="col-sm mt-3 p-5 text-right">
      <h1 id="nombrepizzeria" class="text-right">
      	Pizzeria el Tio Steve
      </h1>
	<p class="text-right"> Realizado por: Andres Euardo Garcia Sosa Codigo: 1151861</p>
    </div>
  </div>        
        <%
            List<Pizza> listaPizzas = (List<Pizza>)(request.getAttribute("listaPizzas"));
            String cantPizzas = request.getParameter("cantPizzas3");
        %>
        <div class="text-center"><p> Numero de pizzas a facturar:</p> <input type="number" readonly='readonly' id="cantPizzas" value="<%= cantPizzas %>"></div>
        <div class="text-center container mt-3 bg-light rounded border border-danger " >
        <table>
            <tr>
                <th>Id Pizza</th>
                <th>Tamaño</th>
                <th>sabor</th>
                <th>precio</th>
            </tr>
        <%    
         for(int i=1;i<=Integer.parseInt(cantPizzas);i++)
         {
             String pizzasTam = request.getParameter("pizzasTam"+i);
             String saborpizza1 = request.getParameter("saborPizza1_"+i);
             String saborpizza2 = request.getParameter("saborPizza2_"+i);
             String ninguno = "Ninguno";
             String numero = Integer.toString(i);
        %>

                <%
                    for(Pizza datos:listaPizzas)
                {    
                    String tipoDescripcion = datos.getIdTipo().getDescripcion();
                    String saborDescripcion = datos.getIdSabor().getDescripcion();
                    %>
        
                    <%
                    
                    if(tipoDescripcion.equals(pizzasTam) && saborDescripcion.equals(saborpizza1))
                    {
                    
                %>
            
            <tr>                
                <td> <%= numero %> </td>
                <td> <%=datos.getIdTipo().getDescripcion() %> </td>
                <td> <%=datos.getIdSabor().getDescripcion()  %> </td>
                <td> <%=datos.getValor()  %> </td>                
            </tr>
            <%
                  }                     
                 }                    
                }     
            %>
            
                </table>
            </div>
        
    </body>
</html>
