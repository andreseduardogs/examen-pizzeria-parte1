<%-- 
    Document   : seleccionarOpciones
    Created on : 20/12/2020, 03:23:44 PM
    Author     : Maggy
--%>

<%@page import="DTO.Sabor"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/bootstrap.min.css" >
        <script src="./js/jsvista.js"></script>
        <title>Pizzeria el Tio Steve</title>
    </head>
    <body style="background-color:#F9CD8E;" onload="definirAdicionales(),definirImagenes()">
        
        <div class="row">
    <div class="col-sm mt-3 pl-5">
    <img src="./imagenes/img1logo.png" width="250" height="204">
    </div>
    <div class="col-sm mt-3 p-5 text-right">
      <h1 id="nombrepizzeria" class="text-right">
      	Pizzeria el Tio Steve
      </h1>
	<p class="text-right"> Realizado por: Andres Euardo Garcia Sosa Codigo: 1151861</p>
    </div>
  </div>
        <form action="Factura" method="post">
     <%
         List<Sabor> sabores = (List<Sabor>)(request.getAttribute("listaSabor"));
         String cantPizzas = request.getParameter("cantPizzas2");
     %>
     <div class="text-center"><p> Total de pizzas</p> <input type="number" readonly='readonly' name="cantPizzas3" id="cantPizzas" value="<%= cantPizzas %>"></div>
     <%    
         for(int i=1;i<=Integer.parseInt(cantPizzas);i++)
         {
             String pizzasTam = request.getParameter("pizzasTam"+i);
     %>
     <div class="container mt-3 bg-light rounded border border-danger" >
         <div class="row"> 
         <div class="col-sm mt-3 pl-5">
             <div id="numPedido<%= i %>"></div>
            <label class=" mt-2 "> Escoja sabores de la pizza <%= i %> (puede escojer 1 o 2):</label>
            <br>
            <label class="mt-2">El tamañno de esta pizza es:</label><input type="text" readonly='readonly' name="pizzasTam<%= i %>" value="<%= pizzasTam %>">
            <br><br>
            <div id="adicionales1_<%= i %>"></div>
            <div id="adicionales2_<%= i %>"></div>
            </div>
            <div class="col-sm mt-3 p-5 text-right"> 
                <select class="text-right" name="saborPizza1_<%= i %>" id="sabores1_<%= i %>" onchange="cambiarAdicionales(<%= i %>),cambiarimagenes(<%= i %>)">
            <%
                for(Sabor datos:sabores)
                {
            %>
                <option> <%= datos.getDescripcion() %> </option>            
            <%
                }
            %>
            </select>
             <select class="text-right" name="saborPizza2_<%= i %>" id="sabores2_<%= i %>" onchange="cambiarAdicionales(<%= i %>),cambiarimagenes(<%= i %>)">
                 <option>Ninguno</option>
            <%
                for(Sabor datos:sabores)
                {
            %>
                <option> <%= datos.getDescripcion() %> </option>            
            <%
                }
            %>
            </select>
            <div class="row">
                <div class="col-sm mt-3 text-right">
            <div id="imagen1_<%= i %>"></div>
                </div>
                <div class="col-sm mt-3 text-right">           
            <div id="imagen2_<%= i %>"></div>
                </div>
            </div>
            </div>
          </div>
            
            
     </div>        
       <%
           }
       %>
     
     <div class="text-center mt-3" id="btnNuevo"> <input class=" bg-warning rounded border-danger" type="submit" value="Calcular factura"> </div> </form>

     	
       
    </body>
</html>
