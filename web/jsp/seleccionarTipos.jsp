<%-- 
    Document   : seleccionarTipos
    Created on : 20/12/2020, 02:34:37 AM
    Author     : Maggy
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet" href="./css/bootstrap.min.css" >
        <script src="./js/jsvista.js"></script>
        <title>Pizzeria el Tio Steve</title>
    </head>
    <body style="background-color:#F9CD8E;" onload="definirTamanos()">
        
        <div class="row">
    <div class="col-sm mt-3 pl-5">
    <img src="./imagenes/img1logo.png" width="250" height="204">
    </div>
    <div class="col-sm mt-3 p-5 text-right">
      <h1 id="nombrepizzeria" class="text-right">
      	Pizzeria el Tio Steve
      </h1>
	<p class="text-right"> Realizado por: Andres Euardo Garcia Sosa Codigo: 1151861</p>
    </div>
  </div>
        
        
        
        <%
            String cantPizzas = request.getParameter("cantPizzas1");
        %>
        
        <div id="contenido">

	<div class="container mt-3 bg-light rounded border border-danger" > 
		<form action="Opciones" method="post">
            <label class=" mt-5 "> Digite cantidad de pizzas:</label> 
            <input class="rounded"type="number" id="entrada" name="cantPizzas2" value="<%= cantPizzas %>"/>
    <input class="rounded border-danger" type="button" id="btnentrada" value="mostrar pizzas" />
	<div id="rtas">	</div>        
            <div id="btnOpciones" class=" mt-3 text-center"> <input type="submit" value="Cargar Opciones">	</div>
         </form>
    <form action="index.html" method="post">	
        <div id="btnNuevo">	<input class=" bg-warning rounded border-danger" type="submit" value="Cargar de nuevo" </div>
	</div></form>
    </div>
    </div>
    </body>
</html>
