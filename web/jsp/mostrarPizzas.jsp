<%-- 
    Document   : mostrarPizzas
    Created on : 19/12/2020, 07:05:01 PM
    Author     : Maggy
--%>

<%@page import="java.util.List"%>
<%@page import="DTO.Pizza"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mostrando pizzas</title>
    </head>
    <body>
        <h1>Mis pizzas</h1>
        
        <%
            List<Pizza> pizzas = (List<Pizza>)(request.getAttribute("lista"));
        %>
        
        <table>
            <tr>
                <th>Id Pizza</th>
                <th>Tamaño</th>
                <th>sabor</th>
                <th>precio</th>
            </tr>
            <%
                for(Pizza datos:pizzas)
                {      
            %>
            <tr>
                
                <td> <%=datos.getIdPizza()%> </td>
                <td> <%=datos.getIdTipo().getDescripcion() %> </td>
                <td> <%=datos.getIdSabor().getDescripcion()  %> </td>
                <td> <%=datos.getValor()  %> </td>
                
            </tr>
            <%
                }     
            %>
            
        </table>
        
    </body>
</html>
