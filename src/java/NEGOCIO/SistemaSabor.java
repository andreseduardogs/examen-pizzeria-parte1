/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NEGOCIO;

import DAO.Conexion;
import DAO.SaborJpaController;
import DTO.Sabor;
import java.util.List;

/**
 *
 * @author Maggy
 */
public class SistemaSabor {

    public SistemaSabor() {
    }
    public List<Sabor> getSabor()
    {
        Conexion con=Conexion.getConexion();
        SaborJpaController saborDAO = new SaborJpaController(con.getBd());
        return saborDAO.findSaborEntities();
    }
}
