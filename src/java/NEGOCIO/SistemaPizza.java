/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NEGOCIO;

import DAO.Conexion;
import DAO.PizzaJpaController;
import DTO.Pizza;
import java.util.List;

/**
 *
 * @author Maggy
 */
public class SistemaPizza {

    public SistemaPizza() {
    }
    
    public List<Pizza> getPizzas()
    {
        Conexion con=Conexion.getConexion();
        PizzaJpaController pizzaDAO = new PizzaJpaController(con.getBd());
        return pizzaDAO.findPizzaEntities();
    }
    
}
