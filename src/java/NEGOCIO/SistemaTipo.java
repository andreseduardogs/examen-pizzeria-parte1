/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package NEGOCIO;

import DAO.Conexion;
import DAO.TipoJpaController;
import DTO.Tipo;
import java.util.List;

/**
 *
 * @author Maggy
 */
public class SistemaTipo {

    public SistemaTipo() {
    }
    public List<Tipo> getTipos()
    {
        Conexion con=Conexion.getConexion();
        TipoJpaController tipoDAO = new TipoJpaController(con.getBd());
        return tipoDAO.findTipoEntities();
    }
}
