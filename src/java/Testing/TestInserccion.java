/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testing;

import DAO.Conexion;
import DAO.SaborJpaController;
import DTO.Sabor;


/**
 *
 * @author Maggy
 */
public class TestInserccion {
    
    public static void main(String[] args) {
        
        Conexion con=Conexion.getConexion();
        SaborJpaController saborDAO = new SaborJpaController(con.getBd());
        Sabor saborNuevo= new Sabor();
        saborNuevo.setIdSabor(2);
        saborNuevo.setDescripcion("pollo, jamon y queso");
            try{
                saborDAO.create(saborNuevo);
                System.out.println("se ha creado un sabor");
            }catch(Exception e){
                System.out.println("No se pudo crear...");  
            }
        
    }
    
}
