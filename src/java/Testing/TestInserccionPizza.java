/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Testing;

import DAO.Conexion;
import DAO.PizzaJpaController;
import DTO.Pizza;
import DTO.Sabor;
import DTO.Tipo;

/**
 *
 * @author Maggy
 */
public class TestInserccionPizza {
    
    public static void main(String[] args) {
        
        Conexion con=Conexion.getConexion();
        PizzaJpaController pizzaDAO = new PizzaJpaController(con.getBd());
        Pizza nuevaPizza = new Pizza();
        nuevaPizza.setValor(20000);
        Tipo tipo = new Tipo();
        tipo.setIdTipo(1);
        Sabor sabor = new Sabor();
        sabor.setIdSabor(2);
        
        nuevaPizza.setIdTipo(tipo);
        nuevaPizza.setIdSabor(sabor);
        nuevaPizza.setIdPizza(3);
        
            try{
                pizzaDAO.create(nuevaPizza);
                System.out.println("se ha creado una nueva pizza");
            }catch(Exception e){
            
                System.out.println("No se puedo crear la pizza, ya existe...");   
            }
        }
    }
    

